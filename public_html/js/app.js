'strict';

//Initialize my app
angular.module('searchApp', ['ngResource', 'ngRoute', 'ngSanitize']);

//Declare routing through the config
angular.module('searchApp')
        .config(function ($routeProvider) {
            $routeProvider
                    .when('/search', {
                        templateUrl: 'index.html',
                        controller: 'searchCtrl'
                    })
                    .otherwise({
                        redirectTo: 'index.html'
                    });
        });

//Declare the factory for my app
angular.module('searchApp')
        .factory('searchFactory', function ($http) {
            return{
                getSearchResults: function (callback) {
                    $http.get('/util/sphider/search.php?query=tiresearch=1').success(callback);
                }
            };
        });

//Declare the controllers
angular.module('searchApp')
        .controller('searchCtrl', function ($scope, $http) {

            $scope.search = function () {
                $scope.results = false;
                $scope.loading = true;
                $http.get('/util/sphider/search.php?query=' + $scope.searchInput + '&search=1&results=100').success(function (response) {

                    $scope.response = response;
                    $scope.result_report = response[0].result_report;
                    $scope.did_you_mean = response[0].did_you_mean;
                    $scope.results = response[0].results;
                    $scope.loading = false;
                });
            };

//            This is the code when there is a suggestion for a misspelled word
            $scope.searchLink = function (url) {
                $scope.results = false;
                $scope.loading = true;
                $scope.keyword = url.split('&')[0];
                $scope.keyword = $scope.keyword.slice(17);
                $scope.searchInput = $scope.keyword;

                $http.get('/util/sphider/' + url + '&results=100').success(function (response) {

                    $scope.response = response;
                    $scope.result_report = response[0].result_report;
                    $scope.did_you_mean = response[0].did_you_mean;
                    $scope.results = response[0].results;
                    $scope.loading = false;
                });
            };

        });